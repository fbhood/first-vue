# Deploy a Vue app to a Raspberry PI Kubernetes cluster

This repo includes a K8s deployment and service objects. To run this code you need a kubernetes cluster up and running and a docker hub account.

## Clone the repo

## Build the docker image and push it to docker-hub

Build the Vue app image and push it to the register. The image was built using Vue.js documentation available [here](https://vuejs.org/v2/cookbook/dockerize-vuejs-app.html#Real-World-Example)

run this command from inside app/

```bash
docker image build --push --platform linux/amd64,linux/arm64 -t pacificdev/qsk-book:1.1.0 .
```

`--platform linux/amd64,linux/arm64` is used to specify multi-architecture image build (it requires buildx installation)

`--push` this option is used to push the image to the registry one built

`-t pacificdev/docker-hub-repo-name-here:1.0.0` is used to tag the build image to version 1.0.0 pushing the image to my Docker hub ID `pacificdev`

## Apply yaml configuration files

This command creates a deployment and a service based on the configuration inside the yml files.

run this comman inside the repo root

```bash
kubectl apply -f deployment.yml -f svc-local.yml
```

## Implement pipelines | Build and push to docker hub

[Docs](https://support.atlassian.com/bitbucket-cloud/docs/build-and-push-a-docker-image-to-a-container-registry/)